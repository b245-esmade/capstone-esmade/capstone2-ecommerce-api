const jwt = require("jsonwebtoken");

const secret = "artShirtKapital";


// Token Creation

module.exports.createAccessToken = (user) =>{
	
	const data = {
		_id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}





// Token Verification

module.exports.verify = (request, response, next) =>{
	
	let token = request.headers.authorization;

	if(typeof token !== "undefined"){
		// Retrieves only token and removes the "Bearer" prefix
		token = token.slice(7, token.length);
		// console.log(token);

		// Validate the token using the "verify" method decrypting the token using the secret code.
		// jwt.verify(token, secretOrPrivateKey, [options/callbackFunction]);
		return jwt.verify(token, secret, (err, data)=>{
			// If JWT is not valid
			if(err){
				return response.send({auth: "You have entered an invalid token. Please verify and try again!"});
			}
			else{
				next();
			}
		})
	}
	// Token does not exist
	else{
		return response.send({auth: "You have entered an invalid token. Please verify and try again!"})
	}
}



// Token Decryption

module.exports.decode = (token) => {
	// Token received is not undefined
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data)=>{
			if(err){
				return null;
			}
			else{
				
				return jwt.decode(token, {complete:true}).payload;
			}
		})
	}
	// Token does not exist (undefined)
	else{
		return null;
	}
}