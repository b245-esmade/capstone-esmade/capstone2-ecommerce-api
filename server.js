
/*
		
		API designed for ArtshirtKapital
			-add users
			-create products, orders
			-retrieve products, orders

*/



//Setup Server
const express = require ('express');
const mongoose = require ('mongoose');

//Setup cors
const cors = require ('cors');

// Routes
const userRoutes = require('./Routes/userRoutes.js')
const productRoutes = require('./Routes/productRoutes.js')
const orderRoutes = require('./Routes/orderRoutes.js')

// Setup Port
let port = 3005;

// Call Express
const app = express();


// Setup ODM
mongoose.set('strictQuery', true),
mongoose.connect('mongodb+srv://admin:admin@batch245-esmade.c4tetod.mongodb.net/API_ecommerce_capstone2?retryWrites=true&w=majority',
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		})


let db = mongoose.connection

db.on('error', console.error.bind(console,'Connection Error'))
db.once('open', ()=> {console.log('We are now connected to the cloud!')})



// Call Middlewares

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(cors());


// Routing
app.use('/user', userRoutes);
app.use('/product', productRoutes);
app.use('/order', orderRoutes);


// Listen
app.listen(port,()=> console.log(`Server is running at port ${port}!`))