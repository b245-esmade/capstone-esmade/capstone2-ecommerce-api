const mongoose  = require('mongoose');
const bcrypt = require('bcrypt');
const auth = require('../auth.js')

const User = require('../Models/userSchema.js');
const Product = require ('../Models/productSchema.js')







// Controllers & Functions

// Register a User

module.exports.userRegistration = (request,response) => {
	const input = request.body;


	// To ensure USER is unique upon Registration
	User.findOne({email: input.email})
	.then(result => {
		if (result !== null){
			return response.send(false)
			// ("Whoa! An account is already registered with your email address. Please try another email.")
		}else {
			let newUser = new User ({
				firstName: input.firstName,
				lastName: input.lastName,
				email: input.email,
				contactNo: input.contactNo,
				password: bcrypt.hashSync(input.password, 10),
				isAdmin: input.isAdmin
			})

			newUser.save()
			.then(save => {
				return response.send(true)
				// ("Great! You are now registered!")
			})
			.catch(error => {
				return response.send(false)//error
			})

		}
	})
	.catch(error => {
		return response.send(false)//error
	})

}



// Authenticate a User

module.exports.userAuthentication =(request, response) => {
	let input = request.body;

	User.findOne({email: input.email})
	.then(result => {
		if (result === null){
			return response.send(false)
			// ("Whoa! unfortunately we could not find an account for this email. Sign up for an account!")
		}else{
			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

				if(isPasswordCorrect){
					return response.send({auth: auth.createAccessToken(result)});
					//token
				}else{
					return response.send(false)
					// ("The password is incorrect. Please try again.")
				}
		}
	})
	.catch(error => {
		return response.send(false);
	})
}



// View ALL User
module.exports.getAllData = (request,response) => {

	User.find({})
	.then(result =>{
		return response.send(result)
	})
	.catch(error => {
		return response.send(false)
	})
}



// View User Profile
module.exports.getProfile = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	// console.log(userData);

	return User.findById(userData._id)
	.then(result =>{
		// avoid to expose sensitive information such as password.
		result.password = "Confidential";

		return response.send(result);
		
	})
}




// Set to Admin
module.exports.setAdmin = (request,response) =>{
	const userId = request.params.userId;
	const input = request.body;
	const userData = auth.decode(request.headers.authorization)

	if(!userData.isAdmin){
		return response.send(false)
		// ("Unauthorized: You do not have permission to perform this action.")
	}else {
		User.findOne({_id:userId})
		.then(result => {
			if (result === null){
				return response.send(false)
				// ("You have entered an invalid user _id. Please verify and try again.")
			}else {
				let updatedUser = {
					isAdmin: input.isAdmin
				}
			
			User.findByIdAndUpdate(userId, updatedUser, {new:true})
			.then(result => {
				// console.log(result)
				response.send(result)})
			.catch(error => response.send(false))
			}
		})
		.catch(error => response.send(false))
	}
}

