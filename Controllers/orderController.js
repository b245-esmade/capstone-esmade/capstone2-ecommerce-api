const mongoose = require('mongoose');
const auth = require('../auth.js');

const Product = require('../Models/productSchema.js');
const Order = require('../Models/orderSchema.js');
const User = require('../Models/userSchema.js');



// Create Order
module.exports.createOrder =  async (request, response) => {
    const userData = auth.decode(request.headers.authorization)
    const input = request.body
    const productId = request.params.productId;


    if(userData.isAdmin) {
        return response.send(false)
        // ("Unauthorized: You do not have permission to perform this action.")
    }else {
        await Product.findById(productId) //target productId
        .then(result => {
            // console.log(result)
            if (result === null){
                response.send(false)
                // ("You have entered an invalid product _id. Please verify and try again.")
            }
            else{
                    let newOrder = new Order(
                        {
                            userId: userData._id,
                            productId: result._id,
                            productName: result.productName,
                            description: result.description,
                            quantity: input.quantity,               
                            totalOrder: result.price*input.quantity //target quantity
                        }
                    )
                     newOrder.save()
                       .then(result => response.send(result))
                        // ("Thanks for your purchase. Your order is being processed for shipping. Track the status here: artshirtkapital.com/myorder/status."))
                       .catch(error => response.send(error)) 
                    
            }
        })
        .catch(error => response.send(error))

    }

}


// View All Orders (Admin Only)

module.exports.allOrders = (request,response) => {
    const userData = auth.decode(request.headers.authorization)
    // console.log(userData)

    if (!userData.isAdmin){
        return response.send(false)
        // ("Unauthorized: You do not have permission to view this directory. If you believe this is an error, please contact the administrator")
    } else {
        Order.find({})
        .then(result => response.send(result))
        .catch(error => response.send(error))
    }

}


// View Order Details (User)

module.exports.orderDetails = (request,response) => {
    const userData = auth.decode(request.headers.authorization);
    const orderId = request.params.orderId;

    Order.findById(orderId)
    .then(result => {
        // console.log(result)
        if(result === null){
            response.send(false)
            // ("You have entered an invalid order _id. Please verify and try again.")
        }
        else {
            if(result.userId === userData._id){
                response.send(result)
            }
            else{
                response.send(false)
            }
        }
    })
    .catch(error => response.send(error))
    
}
