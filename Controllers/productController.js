const mongoose = require('mongoose')
const auth = require('../auth.js')

const Product = require("../Models/productSchema.js");


// Add Product (Admin Only)
module.exports.addProduct = (request,response) => {
	const verifyAdmin = auth.decode(request.headers.authorization);
	// console.log(verifyAdmin);

	if(verifyAdmin.isAdmin === true){
		let input = request.body;
		let newProduct = new Product({

			productName : input.productName,
          	description: input.description,
            category: input.category,
            price: input.price,
            stocks: input.stocks,
            image: input.image
		})

		return newProduct.save()
		.then(result => {
			// console.log(newProduct);
			return response.send(true)
			// ('New product successfully added!')
		})
		.catch(error =>{
			// console.log(error);
			return response.send(false);
		})
	}
	else{
		return response.send(false)
		// ('Unauthorized: You do not have permission to perform this action.')
	}
}


// View ALL Products
module.exports.allProducts = (request,response) => {
    const userData = auth.decode(request.headers.authorization);
    // console.log(userData);

    if (!userData.isAdmin){
        return response.send(false)
        // ("Unauthorized: You do not have permission to view this directory. If you believe this is an error, please contact the administrator")
    }else{
        Product.find({})
        .then(result => response.send(result))
        // able to view all products
        .catch(error => response.send(false));
    }
}


// View Active Products Only

module.exports.activeProducts = (request, response) =>{
	Product.find({isActive: true})
	.then(result => response.send(result))
	// able to view all active products
	.catch(error => response.send(false))
}


// View Product Details

module.exports.productDetails = (request,response) =>{
	const productId = request.params.productId;

	Product.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(error))
}


//Update ProductDetails
module.exports.updateProduct = (request,response) =>{
	const productId = request.params.productId;
	const input = request.body;
	const userData = auth.decode(request.headers.authorization)

	if(!userData.isAdmin){
		return response.send(false)
		// ("Unauthorized: You do not have permission to perform this action.")
	}else {
		Product.findOne({_id: productId})
		.then(result => {
			if (result === null){
				return response.send(false)
				// ("You have entered an invalid product _id. Please verify and try again.")
			}else {
				let updatedProduct = {
					productName: input.productName,
					description: input.description,
					category: input.category,
					price: input.price,
					stocks: input.stocks,
				}
				Product.findByIdAndUpdate(productId, updatedProduct, {new: true})
				.then(result => {
					// console.log(result)
					response.send(result)})
					// find product details
				.catch(error => response.send(false))
			}
		})
		.catch(error => response.send(false))
	}

}


//Archive Product
// module.exports.archiveProduct = (request,response) =>{
// 	const productId = request.params.productId;
// 	const input = request.body;
// 	const userData = auth.decode(request.headers.authorization)

// 	if(!userData.isAdmin){
// 		return response.send(false)
// 		// ("Unauthorized: You do not have permission to perform this action.")
// 	}else {
// 		Product.findOne({_id: productId})
// 		.then(result => {
// 			if (result === null){
// 				return response.send(false)
// 				// ("You have entered and invalid product _id. Please verify and try again.")
// 			}else {
// 				let updatedProduct = {
// 					isActive: input.isActive
// 				}
// 				Product.findByIdAndUpdate(productId, updatedProduct, {new: true})
// 				.then(result => {
// 					// console.log(result)
// 					response.send(result)})
// 				.catch(error => response.send(false))
// 			}
// 		})
// 		.catch(error => response.send(false))
// 	}

// }




module.exports.archiveProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	const productId = request.params.productId
	const input = request.body

	if (!userData.isAdmin) {
		return response.send(false)
	} else {
		Product.findById(productId)
		.then(result => {
			if (result === null) {
				return response.send(false)
			} else  {
			if (result.isActive === true) {
				let unavail = {isActive:false}		
				Product.findByIdAndUpdate(productId, unavail, {new:true})
				.then(result => response.send(true))
				.catch(error => response.send(false))					
			} else {
				let avail = {isActive:true}
				Product.findByIdAndUpdate(productId, avail, {new:true})
				.then(result => response.send(true))
				.catch(error => response.send(false))		
					}		
			} 
		})
		.catch(error => response.send(false))
	}
}
