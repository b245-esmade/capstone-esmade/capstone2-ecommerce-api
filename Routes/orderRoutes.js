const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const orderController = require("../Controllers/orderController");



// ROUTES

// Create Order
router.post("/:productId", auth.verify, orderController.createOrder)

// View ALL Orders
router.get("/all", auth.verify, orderController.allOrders)

// View Order Details
router.get("/details/:orderId",auth.verify, orderController.orderDetails)

// Update Order Details
// router.put("/update/:orderId",auth.verify, orderController.updateOrder)



module.exports = router;

