const express = require ('express');
const router = express.Router()
const auth = require('../auth.js')

const userController = require('../Controllers/userController.js')


// ROUTES

// Add User
router.post('/register', userController.userRegistration)

// Authenticate User 
router.post('/login', userController.userAuthentication)

// View ALL User Data
router.get('/all', userController.getAllData)

// View User Profile
router.get('/details', auth.verify, userController.getProfile);

// Set to Admin
router.patch('/update/:userId', auth.verify, userController.setAdmin)


module.exports = router;