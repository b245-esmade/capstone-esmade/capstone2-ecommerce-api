const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const productController = require("../Controllers/productController");



// ROUTES

// Add Product
router.post('/add', auth.verify, productController.addProduct)

// View ALL Products (Admin Only)
router.get('/all', auth.verify, productController.allProducts)

// View Active Products Only
router.get('/active', productController.activeProducts)

//View Product Details
router.get('/:productId', productController.productDetails)

//Update ProductDetails (Admin Only)
router.put('/update/:productId', auth.verify, productController.updateProduct)

//Archive a Product (Admin Only)
router.patch('/archive/:productId', auth.verify, productController.archiveProduct)



module.exports = router;