const mongoose = require('mongoose');


const orderSchema = new mongoose.Schema({
        userId: {
                type: String,
                required:[true, 'Please enter user _id.']
        },
        productId: {
                type: String,
                required:[true, 'Please enter product _id.']       
        },
        productName: {
                type: String,
                default: 'Artshirt Kapital Merch'
        },
        description:{
                type: String,
                default: 'Artshirt Kapital Merch'
        },
        quantity: {
                type: Number,
                default:1
        },
        purchasedOn: {
                type: Date,
                default: new Date()
        },
        totalOrder: {
                type: Number,
                default: 0
        }
    }
)

module.exports = mongoose.model('Order', orderSchema);
