const mongoose = require ('mongoose');

const productSchema = new mongoose.Schema({
			productName: {
				type: String,
				required: [true, 'Please enter the product name.']
			},
			description:{
				type: String,
				required: [true, 'Please enter product description.']
			},
			category: {
				type: String,
				required:[true, 'Please enter category.']
			},
			isActive: {
				type: Boolean,
				default:true
			},
			price: {
				type: Number,
				required:[true, 'Please enter the price!']
			},
			stocks: {
				type: Number,
				required:[true, 'Please enter number of stocks.']
			},
			image: {
				type: String,
				default: 'http://www.rcdrilling.com/wp-content/uploads/2013/12/default_image_01-1024x1024-570x321.png'
			}

	})


module.exports = mongoose.model('Product', productSchema);