const mongoose = require ('mongoose');

const userSchema = new mongoose.Schema({
		firstName: {
				type: String,
				required: [true, 'Please enter your first name.']
			},
		lastName: {
				type: String,
				required: [true, 'Please enter your last name.']
			},
		contactNo: {
				type:Number,
				required: [true, 'Please enter you contact number']
			},
		email: {
				type: String,
				required: [true, 'Please enter your email address.']
			},
		password: {
				type: String,
				required: [true, 'Please setup your password']
			},
		isAdmin: {
				type: Boolean,
				default: false
			}

})


module.exports = mongoose.model('User', userSchema);